package com.dt.module.form.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dt.module.form.entity.SysForm;
import com.dt.module.form.mapper.SysFormMapper;
import com.dt.module.form.service.ISysFormService;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author algernonking
 * @since 2020-03-28
 */
@Service
public class SysFormServiceImpl extends ServiceImpl<SysFormMapper, SysForm> implements ISysFormService {

}
