package com.dt.module.cmdb.mapper;

import com.dt.module.cmdb.entity.ResHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author algernonking
 * @since 2020-04-16
 */
public interface ResHistoryMapper extends BaseMapper<ResHistory> {

}
