package com.dt.module.cmdb.mapper;

import com.dt.module.cmdb.entity.ResActionItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author algernonking
 * @since 2020-04-07
 */
public interface ResActionItemMapper extends BaseMapper<ResActionItem> {

}
