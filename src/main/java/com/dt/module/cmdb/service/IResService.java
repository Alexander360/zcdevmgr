package com.dt.module.cmdb.service;

import com.dt.module.cmdb.entity.Res;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author algernonking
 * @since 2020-06-27
 */
public interface IResService extends IService<Res> {

}
