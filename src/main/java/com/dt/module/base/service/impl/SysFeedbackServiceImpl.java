package com.dt.module.base.service.impl;

import com.dt.module.base.entity.SysFeedback;
import com.dt.module.base.mapper.SysFeedbackMapper;
import com.dt.module.base.service.ISysFeedbackService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author algernonking
 * @since 2020-05-16
 */
@Service
public class SysFeedbackServiceImpl extends ServiceImpl<SysFeedbackMapper, SysFeedback> implements ISysFeedbackService {

}
