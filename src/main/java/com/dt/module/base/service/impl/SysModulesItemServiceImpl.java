package com.dt.module.base.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dt.module.base.entity.SysModulesItem;
import com.dt.module.base.mapper.SysModulesItemMapper;
import com.dt.module.base.service.ISysModulesItemService;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author algernonking
 * @since 2018-07-30
 */
@Service
public class SysModulesItemServiceImpl extends ServiceImpl<SysModulesItemMapper, SysModulesItem> implements ISysModulesItemService {

}
