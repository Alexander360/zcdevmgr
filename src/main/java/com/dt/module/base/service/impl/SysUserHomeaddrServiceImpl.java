package com.dt.module.base.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dt.module.base.entity.SysUserHomeaddr;
import com.dt.module.base.mapper.SysUserHomeaddrMapper;
import com.dt.module.base.service.ISysUserHomeaddrService;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author algernonking
 * @since 2018-07-27
 */
@Service
public class SysUserHomeaddrServiceImpl extends ServiceImpl<SysUserHomeaddrMapper, SysUserHomeaddr> implements ISysUserHomeaddrService {

}
