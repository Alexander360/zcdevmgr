package com.dt.module.zc.service.impl;

import com.dt.module.zc.entity.ResInout;
import com.dt.module.zc.mapper.ResInoutMapper;
import com.dt.module.zc.service.IResInoutService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author algernonking
 * @since 2020-05-27
 */
@Service
public class ResInoutServiceImpl extends ServiceImpl<ResInoutMapper, ResInout> implements IResInoutService {

}
