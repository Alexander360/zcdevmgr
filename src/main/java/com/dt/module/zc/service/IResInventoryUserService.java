package com.dt.module.zc.service;

import com.dt.module.zc.entity.ResInventoryUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author algernonking
 * @since 2020-05-15
 */
public interface IResInventoryUserService extends IService<ResInventoryUser> {

}
