package com.dt.module.ops.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dt.module.ops.entity.OpsNodeDbdtl;
import com.dt.module.ops.mapper.OpsNodeDbdtlMapper;
import com.dt.module.ops.service.IOpsNodeDbdtlService;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author algernonking
 * @since 2020-01-24
 */
@Service
public class OpsNodeDbdtlServiceImpl extends ServiceImpl<OpsNodeDbdtlMapper, OpsNodeDbdtl> implements IOpsNodeDbdtlService {

}
